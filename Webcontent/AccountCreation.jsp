<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Account creation page</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</head>

<body align="center" class="bg-image"
	style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/76.jpg'); height: 100vh">
	<form method="post" action="validation.jsp">
		<h1>Account creation page</h1>
		<br>
       <%  
       String genderWarning = (String)session.getAttribute("genderError");
       String duplicateMailWarning = (String)session.getAttribute("duplicateMail");
       if(genderWarning != null){
       %>
		<div class="text-danger"> <%=genderWarning %>  </div><br>
		
		<% session.invalidate();}
		if(duplicateMailWarning != null){
		%>
		<div class="text-danger"> <%=duplicateMailWarning %>  </div><br>
		<% session.invalidate();} %>
	
		<table align="center">
			<tr class="form-floating mb-3 mt-3">
				<td>First name</td>
				<td><input type="text" name="FName" required
					class="form-control" placeholder="Enter first Name"></td>
			</tr>
			<tr class="form-floating mb-3 mt-3">
				<td>Last Name</td>
				<td><input type="text" name="LName" required
					class="form-control" placeholder="Enter Last Name"></td>
			</tr>
			<tr class="form-floating mb-3 mt-3">
				<td>Date of Birth</td>
				<td><input type="date" name="DOB" required class="form-control"></td>
			</tr>
			<tr class="form-floating mb-3 mt-3">
				<td>Gender</td>
				<td><input type="radio" name="Gender" value="Male"><span>Male</span>
					<input type="radio" name="Gender" value="Female"><span>Female</span>
				</td>
			</tr>
			<tr class="form-floating mb-3 mt-3">
				<td>Email</td>
				<td><input type="email" name="mail" required
					class="form-control" placeholder="Enter Email"></td>
			</tr>
			<tr class="form-floating mb-3 mt-3">
				<td>password</td>
				<td><input type="password" name="pwd" required
					class="form-control" placeholder="Enter password"></td>
			</tr>
		</table>
		<br> <br>
		<button type="submit" class="btn btn-primary" name="pagedetails" value="accountPage">Create Account</button>

	</form>
</body>
</html>