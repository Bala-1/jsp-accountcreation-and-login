<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="accountCreation.*"%>
	<%@page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

 
 <%
 String srcPage = request.getParameter("pagedetails");

 if(srcPage.equalsIgnoreCase("accountPage")){
	    String firstName = request.getParameter("FName");
		String lastName = request.getParameter("LName");
		String dob = request.getParameter("DOB");
		String gender = request.getParameter("Gender");
		String accMail = request.getParameter("mail");
		String accPwd = request.getParameter("pwd");
		
		
		if(gender == null){
			 session.setAttribute("genderError", "please enter the gender");
			 response.sendRedirect("AccountCreation.jsp");
		}
		else if(accountDb.getAccountsEmailsSet().contains(accMail)){
			session.setAttribute("duplicateMail", "email address is already in use");
			 response.sendRedirect("AccountCreation.jsp");


		}
		else{
			accountDb.addAccount(firstName, lastName, dob, gender, accMail, accPwd);
			session.setAttribute("fname", firstName);
			session.setAttribute("lname", lastName);

			 response.sendRedirect("AccountLogin.jsp");
		}
 }
		
if(srcPage.equalsIgnoreCase("login")){
	String loginMail = request.getParameter("LoginEmail");
	String loginPwd = request.getParameter("LoginPswd");
	
	if(accountDb.getAccountsEmailsSet().contains(loginMail)){
		   if(accountDb.validateUser(loginMail, loginPwd)){
			   
			   String fname=accountDb.getFname(loginMail);
			   String Lname=accountDb.getLname(loginMail);
			   
			   session.setAttribute("fname", fname);
			   session.setAttribute("lname", Lname);
			   
			   response.sendRedirect("HomePage.jsp");

		   }
		   else{
				out.print("Invalid credentials");
				session.setAttribute("invalidPsswd", "Invalid password");
				 response.sendRedirect("AccountLogin.jsp");
		   }
		}
		else{
			session.setAttribute("invalidMail", "Invalid email");
			 response.sendRedirect("AccountLogin.jsp");
		}
}
 %>
 
 
</body>
</html>