<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Account Login page</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</head>
<body>
<div class="container-fluid bg-light bg-image" 
     style="background-image: url('https://thumbs.dreamstime.com/z/beautiful-winter-landscape-scene-background-snow-covered-trees-iced-river-beauty-sunny-winter-backdrop-wonderland-frosty-126334122.jpg');
            height: 100vh">>
        <div class="signInIntro" style="text-align:center ;">
            <h1>Sign in to Account</h1>
        </div>
        
         <%  
       String invalidMail = (String)session.getAttribute("invalidMail");
       String invalidPsswd = (String)session.getAttribute("invalidPsswd");
       if(invalidMail != null){
    	   
       %>
		<div class="text-danger" align="center"> <h1><%=invalidMail %> </h1> </div><br>
		
		<% session.invalidate();}
		if(invalidPsswd != null){
		%>
		<div class="text-danger" align="center"> <h1><%=invalidPsswd %></h1>  </div><br>
		<% session.invalidate();} %>

        <form style=" margin-left: 410px; padding-right: 430px" action="validation.jsp" method="post">
            <div class="form-floating mb-3 mt-3">
                <input type="text" class="form-control" id="email" placeholder="Enter email" name="LoginEmail">
                <label for="email">Email</label>
            </div>
              
            <div class="form-floating mt-3 mb-3">
                <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="LoginPswd">
                <label for="pwd">Password</label>
            </div>
            <div class="form-check mb-3">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" name="remember">Remember me
                </label>
            </div>
            
                <input class="form-check-input" type="hidden" name="pagedetails" value="login">
            
            <button type="submit" class="btn btn-primary" >Log in</button>
        

    </form>

 </div>    
</body>
</html>