package accountCreation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class accountDb {
	static Connection getDBConnection() throws SQLException{
		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		String userName = "system";
		String pwd = "admin@123";
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Connection myConn = DriverManager.getConnection(url, userName, pwd);
		return myConn;
	}
	
	public static void addAccount(String firstName,String lname ,String dob,String gender,String email,String pwd) throws SQLException {
		Connection myConn = getDBConnection();

		String query = "insert into accounts values(?,?,?,?,?,?)";
		
		PreparedStatement pst = myConn.prepareStatement(query);

		pst.setString(1, firstName);
		pst.setString(2, lname);
		pst.setString(3, dob);
		pst.setString(4, gender);
		pst.setString(5, email);
		pst.setString(6, pwd);

		pst.executeUpdate();
	}
	
	public static Set<String> getAccountsEmailsSet() {
		Set<String> set = new HashSet<String>();
		try {
			Connection myConn = getDBConnection();
			
			Statement myStat = myConn.createStatement();
			
			ResultSet rs = myStat.executeQuery("select * from accounts");
			
			 while(rs.next()) {
				 set.add(rs.getString("mail"));
			 }
			 
			// accountDetails.put(rs.getString("mail"), rs.getString("pwd"));
			
			
			
		}catch(Exception e) {
			e.printStackTrace();

		}
		 return set;
	}
	public static  Boolean validateUser(String mail,String pwd) throws SQLException {
		
			Connection myConn = getDBConnection();
			boolean flag = false;
			
			PreparedStatement pst = myConn.prepareStatement("select * from accounts where mail = ?");
			pst.setString(1, mail);

			ResultSet rs = pst.executeQuery();
			
			rs.next();
			if(rs.getString("pwd").equals(pwd)) {
				 flag = true;
			}
			return flag;
		
	}
	
	public static String getFname(String mail) throws SQLException {
		Connection myConn = getDBConnection();
		
		PreparedStatement pst = myConn.prepareStatement("select * from accounts where mail = ?");
		pst.setString(1, mail);

		ResultSet rs = pst.executeQuery();
		
		rs.next();
		return rs.getString("fistName");
		
	}
	
	public static String getLname(String mail) throws SQLException {
		Connection myConn = getDBConnection();
		
		PreparedStatement pst = myConn.prepareStatement("select * from accounts where mail = ?");
		pst.setString(1, mail);

		ResultSet rs = pst.executeQuery();
		
		rs.next();
		return rs.getString("lastName");
		
	}
}
